#!/bin/bash

if [ -f questions.json ]; then
  echo "questions.json already exists. Deleting the existing file..."
  rm questions.json
fi

NEW_CONTENT='[
  {
    "id": "1",
    "title": "How do I reset my password?",
    "content": "To reset your password, go to the login page and click on the \"Forgot Password\" link. Enter your email address associated with your account, and we will send you instructions on how to reset your password.",
    "tags": ["password", "reset", "account", "login", "issues", "security"]
  },
  {
    "id": "2",
    "title": "What is your return policy?",
    "content": "Our return policy allows you to return most items within 30 days of purchase for a full refund. Items must be in their original condition and packaging. Please visit our Returns and Exchanges page for more details.",
    "tags": ["return policy", "refunds", "exchanges", "purchases"]
  },
  {
    "id": "3",
    "title": "How can I track my order?",
    "content": "You can track your order by logging into your account and visiting the \"Order History\" section. Click on the order you want to track, and you will see the tracking details provided by the shipping carrier.",
    "tags": ["order tracking", "shipping", "delivery", "order status"]
  },
  {
    "id": "4",
    "title": "What payment methods do you accept?",
    "content": "We accept various payment methods including credit cards (Visa, MasterCard, American Express), PayPal, and Apple Pay. For more information, please visit our Payment Methods page.",
    "tags": ["payment methods", "billing", "credit cards", "PayPal", "Apple Pay"]
  },
  {
    "id": "5",
    "title": "How do I contact customer support?",
    "content": "You can contact our customer support team via email at support@example.com, by phone at 1-800-123-4567, or through our live chat available on our website. Our support team is available 24/7 to assist you.",
    "tags": ["customer support", "contact", "help", "assistance"]
  },
  {
    "id": "6",
    "title": "Where can I find product manuals?",
    "content": "Product manuals can be found on the product page under the \"Manuals and Guides\" section. You can also search for manuals in the Support section of our website.",
    "tags": ["product manuals", "guides", "support", "instructions"]
  },
  {
    "id": "7",
    "title": "How do I update my shipping address?",
    "content": "To update your shipping address, log into your account and go to the \"Account Settings\" section. From there, you can edit your address information. Make sure to save your changes.",
    "tags": ["shipping address", "update", "account settings", "personal information"]
  },
  {
    "id": "8",
    "title": "Can I change or cancel my order?",
    "content": "Orders can be changed or canceled within 24 hours of placing them. After 24 hours, we are unable to make changes or cancellations as the order is already being processed. Contact customer support for assistance.",
    "tags": ["change order", "cancel order", "order modification", "customer support"]
  },
  {
    "id": "9",
    "title": "What are your business hours?",
    "content": "Our business hours are Monday to Friday, 9 AM to 6 PM. Customer support is available 24/7 via phone and email.",
    "tags": ["business hours", "working hours", "support availability"]
  },
  {
    "id": "10",
    "title": "How do I apply a discount code to my purchase?",
    "content": "During checkout, you will see a field labeled \"Discount Code.\" Enter your code in that field and click \"Apply.\" The discount will be reflected in your order total.",
    "tags": ["discount code", "apply discount", "checkout", "promotions"]
  }
]'


echo "$NEW_CONTENT" > questions.json


echo "questions.json has been updated with new FAQ questions."