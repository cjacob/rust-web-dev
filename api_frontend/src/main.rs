/// Frontend for REST API
/// Display questions, search for questions, add questions, delete questions
/// Interacts with REST API by sending requests to the server
/// as well as receiving responses from the server

/// bound to localhost:3001
mod requests;
mod templates;
use requests::*;
use templates::*;

use askama::Template;
use axum::{
    body::Body,
    http::{header, Method, StatusCode},
    response::Response,
    routing::get,
    Router,
};

use tower_http::cors::{Any, CorsLayer};

/// Entry point for the frontend web server
#[tokio::main]
async fn main() {
    //! # Capabilities
    //! - GET / -> index_handler
    //! - GET /questions -> question_handler
    //! - GET /search -> search_handler
    //! - GET /new -> new_handler
    //! - GET /delete -> delete_handler
    //! - POST /questions -> question_handler_post
    //! - POST /search -> search_handler_post
    //! - POST /new -> new_handler_post
    //! - POST /delete -> delete_handler_post
    //! - Fallback -> 404 not found
    let cors_layer = CorsLayer::new()
        .allow_origin(Any)
        .allow_headers(vec![header::CONTENT_TYPE])
        .allow_methods(vec![Method::GET, Method::POST, Method::DELETE, Method::PUT]);

    let app = Router::new()
        .route("/", get(index_handler))
        .route(
            "/questions",
            get(question_handler).post(|| async { (StatusCode::OK, "post") }),
        )
        .route("/search", get(search_handler).post(search_handler_post))
        .route("/new", get(new_handler).post(new_handler_post))
        .route("/delete", get(delete_handler).post(delete_handler_post))
        .fallback(get(|| async { (StatusCode::NOT_FOUND, "not found bucko") }))
        .layer(cors_layer);

    let listener = tokio::net::TcpListener::bind("127.0.0.1:3001")
        .await
        .unwrap();

    eprintln!("server started: {}", listener.local_addr().unwrap());

    match listener.accept().await {
        Ok((_socket, addr)) => eprintln!("connection established: {}", addr),
        Err(e) => eprintln!("accept failed: {:?}", e),
    }

    // log all requests to eprintln

    axum::serve(listener, app).await.unwrap();
}

async fn question_handler() -> Response<Body> {
    //! sends a GET request to "http://127.0.0.1:3000/questions"
    //! receives a list of questions
    //! renders the question page with the list of questions
    //! path **/questions**

    eprintln!("Request: GET /questions");

    let questions = reqwest::get("http://127.0.0.1:3000/questions")
        .await
        .unwrap()
        .text()
        .await
        .unwrap();

    // eprintln!("Questions: {}", questions);

    let questions: Vec<Question> = serde_json::from_str(&questions).unwrap();

    let template = QuestionTemplate {
        stylesheet: "styles.css".to_string(),
        questions,
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}

async fn index_handler() -> Response<Body> {
    //! Renders the index page
    //! path **/**

    eprintln!("Request: GET /");
    let template = IndexTemplate {
        stylesheet: "styles.css".to_string(),
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}

async fn search_handler() -> Response<Body> {
    //! Renders the search page
    //! path **/search**

    eprintln!("Request: GET /search");
    let template = SearchTemplate {
        stylesheet: "styles.css".to_string(),
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}

async fn new_handler() -> Response<Body> {
    //! Renders the new question page
    //! path **/new**

    eprintln!("Request: GET /new");
    let template = NewTemplate {
        stylesheet: "styles.css".to_string(),
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}

async fn delete_handler() -> Response<Body> {
    //! Renders the delete question page
    //! path **/delete**

    eprintln!("Request: GET /delete");
    let template = DeleteTemplate {
        stylesheet: "styles.css".to_string(),
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}
