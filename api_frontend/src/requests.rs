// manages all the requests to the backend
// parses the request and returns the appropriate response

use std::collections::HashMap;

use crate::templates::*;
use askama::Template;
use axum::{
    body::Body,
    response::{IntoResponse, Response},
    Form, Json,
};
use reqwest::StatusCode;
use serde::{Deserialize, Serialize};

/// Represents a question.
#[derive(Debug, Serialize, Deserialize)]
pub struct Question {
    /// Unique identifier for the question.
    pub id: String,
    /// Title of the question.
    pub title: String,
    /// Answer of the question.
    pub content: String,
    /// Optional tags associated with the question.
    pub tags: Option<Vec<String>>,
}

/// Represents a new question.
#[derive(Debug, Serialize, Deserialize)]
pub struct NewQuestion {
    /// Title of the question.
    pub question: String,
    /// Answer of the question.
    pub answer: String,
    /// Optional tags associated with the question.
    pub tags: Option<String>,
}

pub async fn new_handler_post(Form(new_question): Form<NewQuestion>) -> Response<Body> {
    //! Handles the POST request to create a new question
    //! renders the new question page
    //!
    //! # Returns
    //! Response\<Body\> - The rendered new question page

    // Takes the form data from submitted form of the new question page

    if new_question.question.is_empty() || new_question.answer.is_empty() {
        return Json("Question or answer cannot be empty".to_string()).into_response();
    }

    eprintln!("Request: POST /new");
    eprintln!("\tNew Question: {:?}", new_question);

    let mut params = HashMap::new();
    params.insert("question", new_question.question);
    params.insert("answer", new_question.answer);
    params.insert("tags", new_question.tags.unwrap_or("".to_string()));

    // Sends a POST request to the backend to create a new question
    // only needs the title, content, and tags
    let client = reqwest::Client::new();
    let response = client
        .post("http://127.0.0.1:3000/questions")
        .form(&params)
        .send()
        .await
        .unwrap();

    eprintln!("Response from: {:?}", response.remote_addr());

    if response.status() != StatusCode::OK {
        return Json("Failed to create question".to_string()).into_response();
    }

    let template = QuestionAddedTemplate {
        stylesheet: "styles.css".to_string(),
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}

/// Represents a request to delete a question.
#[derive(Debug, Serialize, Deserialize)]
pub struct DeleteQuestion {
    /// The id of the question to delete.
    pub id: String,
}

pub async fn delete_handler_post(Form(delete_question): Form<DeleteQuestion>) -> Response<Body> {
    //! Handles the POST request to delete a question
    //! renders the delete question page
    //!
    //! # Returns
    //! Response\<Body\> - The rendered delete question page

    // Takes the form data from submitted form of the delete question page

    eprintln!("Request: POST /delete");
    eprintln!("\tDelete Question: {:?}", delete_question);

    let mut params = HashMap::new();
    params.insert("id", delete_question.id);

    // Sends a POST request to the backend to delete a question
    // only needs the id of the question
    let client = reqwest::Client::new();
    let response = client
        .delete("http://127.0.0.1:3000/questions")
        .form(&params)
        .send()
        .await
        .unwrap();

    eprintln!("Response from: {:?}", response.remote_addr());

    if response.status() != StatusCode::OK {
        return Json("Failed to delete question".to_string()).into_response();
    }

    let template = QuestionDeletedTemplate {
        stylesheet: "styles.css".to_string(),
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}

/// Represents a search query of tags.
#[derive(Debug, Serialize, Deserialize)]
pub struct Search {
    /// The query string containing tags to search for.
    pub question: String,
}

pub async fn search_handler_post(Form(search): Form<Search>) -> Response<Body> {
    //! Handles the POST request to search for a question
    //! renders the search page
    //!
    //! # Returns
    //! Response\<Body\> - The rendered search page

    // Takes the form data from submitted form of the search page

    eprintln!("Request: POST /search");
    eprintln!("\tSearch Query: {:?}", search);

    let mut params = HashMap::new();
    params.insert("question", search.question);

    // Sends a POST request to the backend to search for a question
    let client = reqwest::Client::new();
    let response = client
        .post("http://127.0.0.1:3000/search")
        .form(&params)
        .send()
        .await
        .unwrap();

    eprintln!("Response from: {:?}", response.remote_addr());

    if response.status() != StatusCode::OK {
        return Json("Failed to search for question".to_string()).into_response();
    }

    let question = response.text().await.unwrap();
    let question_vec: Vec<Question> = serde_json::from_str(&question).unwrap();

    let template = QuestionTemplate {
        stylesheet: "styles.css".to_string(),
        questions: question_vec,
    };

    let body = template.render().unwrap();
    Response::new(Body::from(body))
}
