//! This module provides template structures for rendering various HTML pages in the application using Askama templates.

use crate::requests::*;
use askama::Template;

/// Template for rendering the index page.
#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
}

/// Template for rendering a page with questions.
#[derive(Template)]
#[template(path = "question.html")]
pub struct QuestionTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
    /// A list of questions to be displayed.
    pub questions: Vec<Question>,
}

/// Template for rendering the delete page.
#[derive(Template)]
#[template(path = "delete.html")]
pub struct DeleteTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
}

/// Template for rendering the search page.
#[derive(Template)]
#[template(path = "search.html")]
pub struct SearchTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
}

/// Template for rendering the new question page.
#[derive(Template)]
#[template(path = "new.html")]
pub struct NewTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
}

/// Template for rendering the question added confirmation page.
#[derive(Template)]
#[template(path = "question_added.html")]
pub struct QuestionAddedTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
}

/// Template for rendering the question deleted confirmation page.
#[derive(Template)]
#[template(path = "question_deleted.html")]
pub struct QuestionDeletedTemplate {
    /// The path to the stylesheet.
    pub stylesheet: String,
}
