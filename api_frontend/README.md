## Jacob Campbell

# Axum Web Dev Project - Spring 2024 - API Frontend

This crate is an example frontend server for the REST API project. It allows users to interact with the API using a web interface. The frontend is built using the `Axum` and uses the `reqwest` crate to make requests to the API. The frontend is limited and does not include any authentication or authorization. This is because the focus of the project is to demonstrate how to build a frontend server using Axum.

Obviously the backend server must be running for this to work.

The default server address is set to `http://127.0.0.1:3001`

## Prerequisites

- [Rust](https://www.rust-lang.org/tools/install)
- [Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)
- [OpenSSL](https://www.openssl.org/) (OpenSSL is required for the `reqwest` crate)

## Installation

Clone the repository:

### bash

```bash
$ git clone https://gitlab.cecs.pdx.edu/cjacob/rust-web-dev.git
$ cd rust-web-dev/api_frontend
```

## Usage

### bash

```bash
$ cargo build
```

Run the server:

### bash

```bash
$ cargo run
```

## Backend API

For the backend API, see the [Backend README](../README.md).

## Capabilities

- Create a new question
- List all questions
- Search for questions by tags
- Delete a question
