//! This module provides a local file-based implementation of a question database and handlers for HTTP requests.
//!     It allows creating, searching, and deleting questions, as well as viewing questions based on certain criteria.
//!     The question database is stored in a JSON file called `questions.json`.

use axum::http::StatusCode;
use axum::{
    extract::{Form, Request},
    response::{IntoResponse, Json},
};
use rand::Rng;
use rusqlite::{params, Connection, Error, Result, Row};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fs::File;
use std::io::{Read, Write};

/// Represents a question.
#[derive(Debug, Clone, Eq, PartialEq, Deserialize, Serialize)]
pub struct Question {
    /// Unique identifier for the question.
    pub id: QuestionId,
    /// Title of the question.
    pub title: String,
    /// Answer of the question.
    pub content: String,
    /// Optional tags associated with the question.
    pub tags: Option<Vec<String>>,
}

/// Unique identifier for a question.
#[derive(Debug, Clone, Deserialize, Serialize, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct QuestionId(pub String);

/// A collection of questions stored in a local file.
#[derive(Debug, Clone, Eq, PartialEq, Deserialize, Serialize)]
pub struct QuestionBase {
    pub questions: HashMap<QuestionId, Question>,
}

impl QuestionBase {
    /// Creates a new instance of `QuestionBase`.
    pub fn new() -> Self {
        QuestionBase {
            questions: Self::init(),
        }
    }

    /// Initializes the `QuestionBase` by reading from the local JSON file.
    fn init() -> HashMap<QuestionId, Question> {
        if !std::path::Path::new("questions.json").exists() {
            let mut file = File::create("questions.json").unwrap();
            file.write_all("[]".as_bytes()).unwrap();
        }

        if !std::path::Path::new("questions.db").exists() {
            let conn = Connection::open("questions.db").unwrap();
            conn.execute(
                "CREATE TABLE questions (
                    id INTEGER PRIMARY KEY,
                    title TEXT NOT NULL,
                    content TEXT NOT NULL,
                    tags TEXT
                )",
                params![],
            )
            .unwrap();

            // move json data to sqlite
            let mut file = File::open("questions.json").unwrap();
            let mut contents = String::new();
            file.read_to_string(&mut contents).unwrap();
            let raw_questions: Vec<Question> = serde_json::from_str(&contents).unwrap();
            for question in raw_questions {
                conn.execute(
                    "INSERT INTO questions (title, content, tags) VALUES (?1, ?2, ?3)",
                    params![
                        question.title.to_string(),
                        question.content.to_string(),
                        question.tags.unwrap_or_default().join(",")
                    ],
                )
                .unwrap();
            }
            sync_json();
        }

        // read from sqlite
        let conn = Connection::open("questions.db").unwrap();
        let mut stmt = conn
            .prepare("SELECT id, title, content, tags FROM questions")
            .unwrap();
        let question_iter = stmt
            .query_map(params![], |row| {
                Ok(Question {
                    id: QuestionId(row.get::<usize, i64>(0).unwrap().to_string()),
                    title: row.get(1).unwrap(),
                    content: row.get(2).unwrap(),
                    tags: get_tags(row).unwrap(),
                })
            })
            .unwrap();

        // take question_iter and create hashmap
        let mut to_questions = HashMap::new();
        for question in question_iter {
            let question = question.unwrap();
            to_questions.insert(question.id.clone(), question);
        }
        to_questions
    }
}

/// Returns the tags associated with a question.
fn get_tags(row: &Row) -> Result<Option<Vec<String>>, Error> {
    row.get::<_, String>(3)
        .map(|tags: String| {
            let tags_vec: Vec<String> = tags.split(',').map(|s| s.to_string()).collect();
            Some(tags_vec)
        })
        .map_err(Into::into)
}

/// Handles HTTP GET requests for fetching questions.
pub async fn question_handler(req: Request) -> impl IntoResponse {
    eprintln!("Request: GET /questions");
    let question_base = QuestionBase::new();
    let params: HashMap<String, String> = req
        .uri()
        .query()
        .map(|q| {
            q.split('&')
                .filter_map(|kv| {
                    let mut split = kv.split('=');
                    if let Some(key) = split.next() {
                        // Check if the key has a corresponding value
                        if let Some(value) = split.next() {
                            Some((key.to_string(), value.to_string()))
                        } else {
                            // If no value, return None
                            Some((key.to_string(), key.to_string()))
                        }
                    } else {
                        // If no key, return None
                        None
                    }
                })
                .collect()
        })
        .unwrap_or_default();
    // eprint!("{:?}", params);
    let questions = get_questions(params, question_base).await;
    Json(questions)
}

/// Fetches questions based on the provided query parameters.

pub async fn get_questions(
    params: HashMap<String, String>,
    question_base: QuestionBase,
) -> Vec<Question> {
    // return json of questions between id range stated in params
    // if no range is stated, return all questions
    if params.is_empty() {
        question_base.questions.values().cloned().collect()
    } else {
        let mut questions = Vec::new();
        let (start_id, end_id) = params.iter().next().unwrap();
        // eprintln!("{}: {}", start_id, end_id);
        let start_id = QuestionId(start_id.to_string());
        let end_id = QuestionId(end_id.to_string());
        for (id, question) in &question_base.questions {
            if id.0.parse::<u32>().unwrap() >= start_id.0.parse::<u32>().unwrap()
                && id.0.parse::<u32>().unwrap() <= end_id.0.parse::<u32>().unwrap()
            {
                questions.push(question.clone());
            }
        }
        questions
    }
}

/// Represents a search query for questions.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct SearchQuery {
    /// The query string containing tags to search for.
    question: String,
}

/// Handles HTTP POST requests for searching questions by tags.
pub async fn search_handler_post(Form(search_query): Form<SearchQuery>) -> impl IntoResponse {
    eprintln!("Request: POST /search");

    // return questions with matching question tags
    // Separate search query into tags with space as delimiter
    let question_base = QuestionBase::new();
    let mut questions = Vec::new();

    let tags_to_find = search_query.question.split(',').collect::<Vec<&str>>();
    let tags_to_find: Vec<&str> = tags_to_find.iter().map(|tag| tag.trim()).collect();

    for question in question_base.questions.values() {
        if let Some(tags) = &question.tags {
            for tag in tags {
                if tags_to_find.contains(&tag.as_str()) {
                    questions.push(question.clone());
                    break;
                }
            }
        }
    }

    Json(if questions.is_empty() {
        vec![Question {
            id: QuestionId("404".to_string()),
            title: "404".to_string(),
            content: "Question not found".to_string(),
            tags: None,
        }]
    } else {
        questions
    })
}

/// Represents a new question to be added.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct NewQuestion {
    /// The question text.
    question: String,
    /// The answer to the question.
    answer: String,
    /// Optional tags associated with the question.
    tags: Option<String>,
}

/// Handles HTTP POST requests for adding new questions.
pub async fn new_handler_post(Form(new_question): Form<NewQuestion>) -> impl IntoResponse {
    // question or answer is empty, return error
    if new_question.question.is_empty() || new_question.answer.is_empty() {
        return Json("Question or answer cannot be empty".to_string());
    }

    // if there are tags, split them into a vector, separated by comma
    let tags = new_question
        .tags
        .map(|tags| tags.split(',').map(|s| s.to_string()).collect());

    let new_question = Question {
        id: QuestionId("1".to_string()),
        title: new_question.question,
        content: new_question.answer,
        tags,
    };

    // write to sqlite
    let conn = Connection::open("questions.db").unwrap();
    conn.execute(
        "INSERT INTO questions (title, content, tags) VALUES (?1, ?2, ?3)",
        params![
            new_question.title.to_string(),
            new_question.content.to_string(),
            new_question.tags.unwrap_or_default().join(",")
        ],
    )
    .unwrap();

    // JSON FILE
    sync_json();

    Json("Question added successfully".to_string())
}

/// Represents a request to delete a question.
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DeleteQuestion {
    /// The id of the question to delete.
    id: String,
}

/// Handles HTTP POST requests for deleting questions.
pub async fn delete_handler_post(Form(delete_question): Form<DeleteQuestion>) -> impl IntoResponse {
    eprintln!("Request: DELETE /questions");

    let mut question_base = QuestionBase::new();
    let id = QuestionId(delete_question.id);

    if question_base.questions.remove(&id).is_some() {
        // delete from sqlite
        let conn = Connection::open("questions.db").unwrap();
        conn.execute(
            "DELETE FROM questions WHERE id = ?1",
            params![id.0.parse::<i32>().unwrap()],
        )
        .unwrap();

        sync_json();

        (StatusCode::OK, "Question deleted successfully".to_string())
    } else {
        (StatusCode::NOT_FOUND, "Question not found".to_string())
    }
}

/// Generates a random question identifier from the `QuestionBase`.
/// Used for viewing a random question.
pub fn random_question_id() -> QuestionId {
    let question_base = QuestionBase::new();
    let mut rng = rand::thread_rng();
    let mut id: QuestionId;
    // check if question_base is empty
    if question_base.questions.is_empty() {
        id = QuestionId("0".to_string());
    } else {
        let random_id = rng.gen_range(0..question_base.questions.len());
        id = QuestionId(random_id.to_string());
        while !question_base.questions.contains_key(&id) {
            id = QuestionId(rng.gen_range(0..question_base.questions.len()).to_string());
        }
    }
    id
}

fn sync_json() {
    let question_base = QuestionBase::new();
    let mut file = File::create("questions.json").unwrap();
    let json = serde_json::to_string(&question_base.questions.values().collect::<Vec<&Question>>())
        .unwrap();
    file.write_all(json.as_bytes()).unwrap();
}
