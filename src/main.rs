mod question;
use question::*;

use axum::{
    extract::{Form, Json},
    http::{header, Method, StatusCode},
    response::IntoResponse,
    routing::{get, post},
    Router,
};

use tower_http::cors::{Any, CorsLayer};

/// Entry point for the backend web server
#[tokio::main]
async fn main() {
    //! # Routes
    //!
    //! - GET / -> index_handler
    //! - GET /questions -> question_handler
    //! - POST /questions -> question_handler_post
    //! - DELETE /questions -> delete_question_handler
    //! - POST /search -> search_handler_post
    //! - Fallback -> 404 not found
    let cors_layer = CorsLayer::new()
        .allow_origin(Any)
        .allow_headers(vec![header::CONTENT_TYPE])
        .allow_methods(vec![Method::GET, Method::POST, Method::DELETE, Method::PUT]);

    let app = Router::new()
        .route("/", get(index_handler))
        .route(
            "/questions",
            get(question_handler)
                .post(question_handler_post)
                .delete(delete_question_handler),
        )
        .route("/search", post(search_handler_post))
        .fallback(get(|| async { (StatusCode::NOT_FOUND, "not found bucko") }))
        .layer(cors_layer);

    let listener = tokio::net::TcpListener::bind("127.0.0.1:3000")
        .await
        .unwrap();

    eprintln!("server started: {}", listener.local_addr().unwrap());

    match listener.accept().await {
        Ok((_socket, addr)) => eprintln!("connection established: {}", addr),
        Err(e) => eprintln!("accept failed: {:?}", e),
    }

    axum::serve(listener, app).await.unwrap();
}

/// Handles HTTP GET requests for the index.
async fn index_handler() -> impl IntoResponse {
    eprintln!("Request: GET /");
    let rand_id = random_question_id();

    Json(rand_id)
}

/// Handles HTTP POST requests for adding questions.
async fn question_handler_post(Form(new_question): Form<NewQuestion>) -> impl IntoResponse {
    eprintln!("Request: POST /questions");
    new_handler_post(Form(new_question)).await
}

/// Handles HTTP DELETE requests for deleting questions.
async fn delete_question_handler(Form(delete_question): Form<DeleteQuestion>) -> impl IntoResponse {
    eprintln!("Request: DELETE /questions");
    delete_handler_post(Form(delete_question)).await
}
