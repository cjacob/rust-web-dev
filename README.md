## Jacob Campbell

# Axum Web Dev Project - Spring 2024

This is an example project for the Rust web development class at Portland State University, Spring 2024. It demonstrates how to build a REST API using the Axum web framework in Rust. The project consists of an API for managing and retrieving questions, allowing users to create, read, and delete questions stored in a JSON file. It is limited and does not include any authentication or authorization. This is because the focus of the project is to demonstrate how to build a REST API using Axum.

The default server address is set to `http://127.0.0.1:3000`

## Prerequisites

- [Rust](https://www.rust-lang.org/tools/install)
- [Cargo](https://doc.rust-lang.org/cargo/getting-started/installation.html)

## Installation

Clone the repository:

### bash

```bash
$ git clone https://gitlab.cecs.pdx.edu/cjacob/rust-web-dev.git
$ cd rust-web-dev
```

## Usage

Build the project:

### bash

```bash
$ cargo build
```

Run the server:

### bash

```bash
$ cargo run
```

## Current Implementation

The webserver is a simple REST API that allows users to manage questions stored in a SQLite database. The questions are also stored in a local JSON file called `questions.json`. If there is no database file, the server will create one and try to load the questions from the JSON file. The server will then be able to create, read, and delete questions from the database.

If a `questions.json` is not present, then one will be created as an empty set. The server will then be able to create, read, and delete questions from the file.

If you want to preload the server with some questions, you can run the following shell script:

```bash
$ ./create_questions.sh
$ questions.json has been updated with new FAQ questions.
```

The server will also be able to search for questions based on tags. The tags are stored as a comma-separated list in the database file. The server will be able to search for questions based on the tags provided in the search query.

The database table is structured as

```sql
CREATE TABLE questions (
    id INTEGER PRIMARY KEY,
    title TEXT NOT NULL,
    content TEXT NOT NULL,
    tags TEXT
);
```

A Question is structured as

```json
{
  "id": "02",
  "title": "What is your favorite color?",
  "content": "Blue",
  "tags": ["test"]
}
```

## Endpoints

The following endpoints are available:

- `GET /` - Retrieves a home page (for backend will return a randid used in earlier implementation)
- `GET /questions` - Retrieves all questions
- `GET /questions?{id}` - Retrieves a question by id
- `POST /questions` - Creates a new question
- `DELETE /questions` - with correct form data will delete a question matching id
- `POST /search` - with correct form data will search for a question matching tags. (tags seperated by commas)

## Example with `curl`

```bash
$ curl -X GET localhost:3000/questions
```

```bash
$ curl -X GET localhost:3000/questions?2
```

```bash
$ curl -X POST localhost:3000/questions -d "id=03&title=What is your favorite food?&content=Pizza&tags=test"
```

```bash
$ curl -X DELETE localhost:3000/questions -d "id=03"
```

```bash
$ curl -X GET localhost:3000
```
